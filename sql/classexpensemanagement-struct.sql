/*
 Navicat Premium Data Transfer

 Source Server         : root@localhost-123456
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3306
 Source Schema         : classexpensemanagement

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 02/07/2023 18:12:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for backadmin
-- ----------------------------
DROP TABLE IF EXISTS `backadmin`;
CREATE TABLE `backadmin`  (
  `AdminID` int NOT NULL AUTO_INCREMENT COMMENT '管理员ID',
  `AdminName` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '管理员名',
  `AdminAccount` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '管理员账号',
  `AdminPWD` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '管理员密码，sha加密存储',
  `AdminPhone` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '手机号',
  `AdminEmail` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '无' COMMENT '邮箱',
  PRIMARY KEY (`AdminID`) USING BTREE,
  UNIQUE INDEX `AdminAccount`(`AdminAccount` ASC) USING BTREE COMMENT '管理员账号',
  UNIQUE INDEX `AdminPhone`(`AdminPhone` ASC) USING BTREE COMMENT '手机号'
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for class
-- ----------------------------
DROP TABLE IF EXISTS `class`;
CREATE TABLE `class`  (
  `ClassID` int NOT NULL AUTO_INCREMENT COMMENT '班级ID',
  `ClassName` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '班级名',
  `ClassSize` int NOT NULL DEFAULT 1 COMMENT '班级人数',
  `IsBan` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '否' COMMENT '是否禁封（否/是）',
  `ClassExBala` float(7, 2) NOT NULL DEFAULT 0.00 COMMENT '班费余额',
  PRIMARY KEY (`ClassID`) USING BTREE,
  UNIQUE INDEX `className`(`ClassName` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for classadmin
-- ----------------------------
DROP TABLE IF EXISTS `classadmin`;
CREATE TABLE `classadmin`  (
  `ClassAdminID` int NOT NULL AUTO_INCREMENT COMMENT '班级管理员ID',
  `StartTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '上任时间',
  `EndTime` timestamp NULL DEFAULT NULL COMMENT '卸任时间',
  `CA_UserID` int NOT NULL COMMENT '用户ID',
  `CA_ClassID` int NOT NULL COMMENT '班级ID',
  PRIMARY KEY (`ClassAdminID`) USING BTREE,
  INDEX `FK_CA_UserID`(`CA_UserID` ASC) USING BTREE,
  INDEX `FK_CA_ClassID`(`CA_ClassID` ASC) USING BTREE,
  CONSTRAINT `classadmin_ibfk_1` FOREIGN KEY (`CA_ClassID`) REFERENCES `class` (`ClassID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `classadmin_ibfk_2` FOREIGN KEY (`CA_UserID`) REFERENCES `userinfo` (`UserID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for classnotice
-- ----------------------------
DROP TABLE IF EXISTS `classnotice`;
CREATE TABLE `classnotice`  (
  `ClaNoticeID` int NOT NULL AUTO_INCREMENT COMMENT '通知ID',
  `NoticeTitle` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '通知标题',
  `NoticeContent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '通知内容',
  `ReleaseTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '发布时间',
  `CN_ClassAdminID` int NOT NULL COMMENT '班级管理员ID',
  PRIMARY KEY (`ClaNoticeID`) USING BTREE,
  INDEX `FK_CN_ClassAdminID`(`CN_ClassAdminID` ASC) USING BTREE,
  CONSTRAINT `classnotice_ibfk_1` FOREIGN KEY (`CN_ClassAdminID`) REFERENCES `classadmin` (`ClassAdminID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for expenseapply
-- ----------------------------
DROP TABLE IF EXISTS `expenseapply`;
CREATE TABLE `expenseapply`  (
  `ExpApplyID` int NOT NULL AUTO_INCREMENT COMMENT '记录ID',
  `ApplyAmount` float NOT NULL COMMENT '申请金额',
  `ApplyTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '申请时间',
  `AuditTime` timestamp NULL DEFAULT NULL COMMENT '审核时间',
  `ApplyReason` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '申请原因',
  `status` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '审核中' COMMENT '状态(审核中/不同意/同意/撤销)',
  `BeforeClassExp` float(7, 2) NULL DEFAULT NULL COMMENT '审核时班费金额',
  `EA_UserID` int NOT NULL COMMENT '用户ID',
  `EA_ClassAdminID` int NOT NULL COMMENT '班级管理员ID',
  PRIMARY KEY (`ExpApplyID`) USING BTREE,
  INDEX `FK_EA_UserID`(`EA_UserID` ASC) USING BTREE,
  INDEX `FK_EA_ClassAdminID`(`EA_ClassAdminID` ASC) USING BTREE,
  CONSTRAINT `expenseapply_ibfk_1` FOREIGN KEY (`EA_ClassAdminID`) REFERENCES `classadmin` (`ClassAdminID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `expenseapply_ibfk_2` FOREIGN KEY (`EA_UserID`) REFERENCES `userinfo` (`UserID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice`  (
  `NoticeID` int NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `NoticeTitle` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '公告标题',
  `NoticeContent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '公告内容',
  `ReleaseTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '发布时间',
  `N_AdminID` int NOT NULL COMMENT '后台管理员ID',
  PRIMARY KEY (`NoticeID`) USING BTREE,
  INDEX `FK_AdminID`(`N_AdminID` ASC) USING BTREE,
  CONSTRAINT `notice_ibfk_1` FOREIGN KEY (`N_AdminID`) REFERENCES `backadmin` (`AdminID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for opclassrecord
-- ----------------------------
DROP TABLE IF EXISTS `opclassrecord`;
CREATE TABLE `opclassrecord`  (
  `RecordID` int NOT NULL AUTO_INCREMENT COMMENT '记录ID',
  `OpTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  `OUP_COpType` int NOT NULL COMMENT '班级管理类型ID',
  `OCR_ClassID` int NOT NULL COMMENT '班级ID',
  `OCR_BackAdminID` int NOT NULL COMMENT '后台管理员ID',
  PRIMARY KEY (`RecordID`) USING BTREE,
  INDEX `FK_OUP_COpType`(`OUP_COpType` ASC) USING BTREE,
  INDEX `FK_OCR_ClassID`(`OCR_ClassID` ASC) USING BTREE,
  INDEX `FK_OCR_BackAdminID`(`OCR_BackAdminID` ASC) USING BTREE,
  CONSTRAINT `opclassrecord_ibfk_1` FOREIGN KEY (`OCR_BackAdminID`) REFERENCES `backadmin` (`AdminID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `opclassrecord_ibfk_2` FOREIGN KEY (`OCR_ClassID`) REFERENCES `class` (`ClassID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `opclassrecord_ibfk_3` FOREIGN KEY (`OUP_COpType`) REFERENCES `opclasstype` (`OpClassTypeID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for opclasstype
-- ----------------------------
DROP TABLE IF EXISTS `opclasstype`;
CREATE TABLE `opclasstype`  (
  `OpClassTypeID` int NOT NULL AUTO_INCREMENT COMMENT '班级管理类型ID',
  `TypeName` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '类型名',
  `durationDay` int NULL DEFAULT 0 COMMENT '持续天数',
  PRIMARY KEY (`OpClassTypeID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for opuserrecord
-- ----------------------------
DROP TABLE IF EXISTS `opuserrecord`;
CREATE TABLE `opuserrecord`  (
  `RecordID` int NOT NULL AUTO_INCREMENT COMMENT '记录ID',
  `OpTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  `OUP_UOpType` int NOT NULL COMMENT '用户管理类型ID',
  `OUR_UserID` int NOT NULL COMMENT '用户ID',
  `OUR_BackAdminID` int NOT NULL COMMENT '后台管理员ID',
  PRIMARY KEY (`RecordID`) USING BTREE,
  INDEX `FK_OUR_UserID`(`OUR_UserID` ASC) USING BTREE,
  INDEX `FK_OUR_BackAdminID`(`OUR_BackAdminID` ASC) USING BTREE,
  INDEX `FK_OUP_UOpType`(`OUP_UOpType` ASC) USING BTREE,
  CONSTRAINT `opuserrecord_ibfk_1` FOREIGN KEY (`OUP_UOpType`) REFERENCES `opusertype` (`OpUserTypeID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `opuserrecord_ibfk_2` FOREIGN KEY (`OUR_BackAdminID`) REFERENCES `backadmin` (`AdminID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `opuserrecord_ibfk_3` FOREIGN KEY (`OUR_UserID`) REFERENCES `userinfo` (`UserID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for opusertype
-- ----------------------------
DROP TABLE IF EXISTS `opusertype`;
CREATE TABLE `opusertype`  (
  `OpUserTypeID` int NOT NULL AUTO_INCREMENT COMMENT '用户管理类型ID',
  `TypeName` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '类型名',
  `DurationDay` int NULL DEFAULT 0 COMMENT '持续天数',
  PRIMARY KEY (`OpUserTypeID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for paymentproject
-- ----------------------------
DROP TABLE IF EXISTS `paymentproject`;
CREATE TABLE `paymentproject`  (
  `PayProjID` int NOT NULL AUTO_INCREMENT COMMENT '项目ID',
  `PayProjTitle` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '标题',
  `PayProjBrief` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '简介',
  `PayProjAmount` float(7, 2) NOT NULL DEFAULT 0.00 COMMENT '缴费金额',
  `ReleaseTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '发布时间',
  `Deadline` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '截止时间',
  `IsDelete` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '否' COMMENT '是否删除（否/是）',
  `PP_ClassAdminID` int NOT NULL COMMENT '班级管理员ID',
  PRIMARY KEY (`PayProjID`) USING BTREE,
  INDEX `FK_PR_ClassAdminID`(`PP_ClassAdminID` ASC) USING BTREE,
  CONSTRAINT `paymentproject_ibfk_1` FOREIGN KEY (`PP_ClassAdminID`) REFERENCES `classadmin` (`ClassAdminID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for paymentrecord
-- ----------------------------
DROP TABLE IF EXISTS `paymentrecord`;
CREATE TABLE `paymentrecord`  (
  `PayRecTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '缴费时间',
  `BeforeClassExp` float(7, 2) NOT NULL COMMENT '缴费前班费金额',
  `PR_UserID` int NOT NULL COMMENT '缴费用户ID,共同主键',
  `PR_PayProjID` int NOT NULL COMMENT '缴费项目ID,共同主键',
  PRIMARY KEY (`PR_UserID`, `PR_PayProjID`) USING BTREE,
  INDEX `FK_PR_PayProjID`(`PR_PayProjID` ASC) USING BTREE,
  INDEX `FK_PR_UserID`(`PR_UserID` ASC) USING BTREE,
  CONSTRAINT `paymentrecord_ibfk_1` FOREIGN KEY (`PR_PayProjID`) REFERENCES `paymentproject` (`PayProjID`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `paymentrecord_ibfk_2` FOREIGN KEY (`PR_UserID`) REFERENCES `userinfo` (`UserID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Table structure for userinfo
-- ----------------------------
DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE `userinfo`  (
  `UserID` int NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `UserName` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '用户名',
  `UserAccount` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '账号',
  `UserPWD` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '密码，sha加密存储',
  `UserPhone` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '手机号',
  `UserEmail` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '无' COMMENT '邮箱',
  `IsBan` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '否' COMMENT '是否禁封（否/是）',
  `UI_ClassID` int NULL DEFAULT NULL COMMENT '班级ID',
  PRIMARY KEY (`UserID`) USING BTREE,
  UNIQUE INDEX `UserAccount`(`UserAccount` ASC) USING BTREE,
  UNIQUE INDEX `UserPhone`(`UserPhone` ASC) USING BTREE,
  INDEX `FK_ClassID`(`UI_ClassID` ASC) USING BTREE,
  CONSTRAINT `userinfo_ibfk_1` FOREIGN KEY (`UI_ClassID`) REFERENCES `class` (`ClassID`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 69 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- View structure for jfxm
-- ----------------------------
DROP VIEW IF EXISTS `jfxm`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `jfxm` AS select `paymentproject`.`PayProjID` AS `payProjID`,`class`.`ClassID` AS `classID`,`userinfo`.`UserID` AS `releaseUserID`,`class`.`ClassName` AS `className`,`userinfo`.`UserName` AS `releaseUser`,`paymentproject`.`PayProjTitle` AS `payProjTitle`,`paymentproject`.`PayProjBrief` AS `payProjBrief`,`paymentproject`.`PayProjAmount` AS `payProjAmount`,`paymentproject`.`ReleaseTime` AS `releaseTime`,`paymentproject`.`Deadline` AS `deadLine`,`paymentproject`.`IsDelete` AS `isDelete` from (((`paymentproject` join `classadmin`) join `userinfo`) join `class`) where ((`paymentproject`.`PP_ClassAdminID` = `classadmin`.`ClassAdminID`) and (`classadmin`.`CA_UserID` = `userinfo`.`UserID`) and (`classadmin`.`CA_ClassID` = `class`.`ClassID`));

SET FOREIGN_KEY_CHECKS = 1;
